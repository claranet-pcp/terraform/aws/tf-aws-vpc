#vpc module

resource "aws_vpc" "vpc" {

  cidr_block                = var.vpc_cidr
  enable_dns_hostnames      = var.enable_dns_hostnames
  enable_dns_support        = var.enable_dns_support

  tags = {
    Name                    = "${var.customer}-${var.envname}"
    Environment             = var.envname
    EnvType                 = var.envtype
    }
}

resource "aws_vpc_dhcp_options" "vpc" {
  domain_name           = var.domain_name
  domain_name_servers   = var.domain_name_servers
  ntp_servers           = var.ntp_servers
  netbios_name_servers  = var.netbios_name_servers
  netbios_node_type     = var.netbios_node_type

  tags = {
    Name                    = "${var.customer}-${var.envname}"
    Environment             = var.envname
    EnvType                 = var.envtype
    }
}

resource "aws_vpc_dhcp_options_association" "vpc_dhcp" {
  vpc_id          = aws_vpc.vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.vpc.id
}

resource "aws_internet_gateway" "igw" {

  vpc_id                    = aws_vpc.vpc.id

  tags = {
    Name                    = "${var.customer}-${var.envname}"
    Environment             = var.envname
    EnvType                 = var.envtype
    }
}

#public resources

resource "aws_subnet" "public" {

  vpc_id                    = aws_vpc.vpc.id
  cidr_block                = var.public_subnets[count.index]
  availability_zone         = var.azs[count.index]
  map_public_ip_on_launch   = var.map_public_ip_on_launch
  count                     = length(var.public_subnets)

  tags = {
    Name                    = "${var.customer}-${var.envname}-public-${element(var.azs, count.index)}"
    Environment             = var.envname
    EnvType                 = var.envtype
    }
}

resource "aws_route_table" "public" {

  vpc_id                    = aws_vpc.vpc.id
  propagating_vgws          = var.public_propagating_vgws

  tags = {
    Name                    = "${var.customer}-${var.envname}-public"
    Environment             = var.envname
    EnvType                 = var.envtype
    }
}

resource "aws_route_table_association" "public" {

  subnet_id      = aws_subnet.public.*.id[count.index]
  route_table_id = aws_route_table.public.id
  count          = length(var.public_subnets)
}

resource "aws_route" "public_internet_gateway" {

  route_table_id            = aws_route_table.public.id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = aws_internet_gateway.igw.id
}

#private resources

resource "aws_subnet" "private" {

  vpc_id                    = aws_vpc.vpc.id
  cidr_block                = var.private_subnets[count.index]
  availability_zone         = var.azs[count.index]
  count                     = length(var.private_subnets)

  tags = {
    Name                    = "${var.customer}-${var.envname}-private-${element(var.azs, count.index)}"
    Environment             = var.envname
    EnvType                 = var.envtype
    }
}

resource "aws_route_table" "private" {

  vpc_id                    = aws_vpc.vpc.id
  count                     = length(var.private_subnets)
  propagating_vgws          = var.private_propagating_vgws

  tags = {
    Name                    = "${var.customer}-${var.envname}-private-${element(var.azs, count.index)}"
    Environment             = var.envname
    EnvType                 = var.envtype
    }
}

resource "aws_route_table_association" "private" {

  subnet_id                 = aws_subnet.private.*.id[count.index]
  route_table_id            = aws_route_table.private.*.id[count.index]
  count                     = length(var.private_subnets)
}
