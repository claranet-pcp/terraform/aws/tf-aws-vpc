## vpc outputs

output "vpc_id" {
  value = aws_vpc.vpc.id
}
output "vpc_cidr" {
  value = aws_vpc.vpc.cidr_block
}
output "availability_zones" {
  value = aws_subnet.public.*.availability_zone
}

## Subnet Outputs

output "public_subnets" {
  value = aws_subnet.public.*.id
}

output "public_route_tables" {
  value = aws_route_table.public.*.id
}

output "private_subnets" {
  value = aws_subnet.private.*.id
}

output "private_route_tables" {
  value = aws_route_table.private.*.id
}

output "igw_id" {
  value = aws_internet_gateway.igw.id
}