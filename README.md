tf-aws-vpc
==========

VPC with Public and Private Subnets - Terraform Module

# Terraform Version Compatibility
| Module version | Terraform version |
|----------------|-------------------|
| 1.x.x          | 0.12.x            |
| 0.x.x          | 0.11.x            |

Usage
-----

```js
module "vpc" {
  source                    = "git::ssh://git@gogs.bashton.net/Bashton-Terraform-Modules/tf-aws-vpc.git?ref=v0.0.3"
  customer                  = "mr_customer"
  aws_region                = "eu-west-2"
  vpc_cidr                  = "10.0.0.0/16"
  public_subnets            = "10.0.1.0/24,10.0.2.0/24"
  private_subnets           = "10.0.11.0/24,10.0.12.0/24"
  azs                       = "eu-west-2a,eu-west-2b"
  map_public_ip_on_launch   = "false"
  allowed_ssh_cidrs         = "88.97.72.136/32,54.76.122.23/32"
  domain_name               = "vpc.example.com"
  domain_name_servers       = ["127.0.0.1","AmazonProvidedDNS"]
  ntp_servers               = ["10.0.11.10","10.0.11.11"]
  netbios_name_servers      = ["10.0.11.10","10.0.11.11"]
  netbios_node_type         = 2
  enable_dns_hostnames      = "false"
  enable_dns_support        = "true"
  public_propagating_vgws   = ["${data.aws_vpn_gateway.selected.id}"]
  private_propagating_vgws  = ["${data.aws_vpn_gateway.selected.id}"]
}
```

Variables
---------

 - `customer` - name of the VPC
 - `vpc_cidr` - CIDR block of the VPC
 - `public_subnets` - comma separated list of public subnet cidrs
 - `private_subnets` - comma separated list of nat subnet cidrs
 - `aws_region` - name of aws region
 - `azs` - comma separated lists of AZs in which to distribute subnets
 - `map_public_ip_on_launch` - flag to map public IP address to machines on public subnets
 - `allowed_ssh_cidrs` - comma separated list of allowed network ranges for SSH access into the resources within the VPC
 - `domain_name` - comma separated list of domain names to be used within the VPC
 - `domain_name_servers` - comma separated list of domain name servers within the VPC
 - `ntp_servers` - comma separated list of NTP servers
 - `netbios_name_servers` - comma separated list of NETBIOS name servers
 - `netbios_node_type` - NETBIOS node type. AWS recommends to specify 2 since broadcast and multicast are not supported
 - `enable_dns_hostnames` - true or false
 - `enable_dns_support` - true or false   
 - `public_propagating_vgws` - A list of VGWs for which their routes should be allowed to propagate to the public route table(s)
 - `private_propagating_vgws` - A list of VGWs for which their routes should be allowed to propagate to the private route table(s)

It's generally preferable to keep `public_subnets`, `private_subnets`, and
`azs` to lists of the same length.

Outputs
-------

 - `vpc_id` - the VPC id
 - `vpc_cidr` - assigned CIDR block of the VPC
 - `availability_zones` - comma separated list of availability zones
 - `public_subnets` - comma separated list of public subnet ids
 - `public_route_tables` - comma separated list of public route table ids
 - `private_subnets` - comma separated list of private subnet ids
 - `private_route_tables` - comma separated list of private route table ids
 - `igw_id` - the IGW id