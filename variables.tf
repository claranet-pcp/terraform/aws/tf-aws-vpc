/* Environment variables */
variable "customer" {
  description = "The value that will become the first prefix on the 'Name' tag on resources created by this module"
  type = string
}

variable "envname" {
  description = "The value that will become the second prefix on the 'Name' tag on resources created by this module, and the value for the 'Environment' tag"
  type = string
}

variable "envtype" {
  description = "The value that will populate the 'EnvType' tag"
  type = string
  default = ""
}

variable "aws_region" {
  description = "The AWS region in which to create the VPC"
  type = string
  default = "eu-west-1"
}

variable "azs" {
  description = "A list of Availability zones in the chosen region"
  type = list(string)
  default = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}

/* VPC variables */
variable "vpc_cidr" {
  description = "The CIDR block for the VPC"
  type = string
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC."
  type = list(string)
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC."
  type = list(string)
}

variable "private_propagating_vgws" {
  description = "A list of VGWs for which their routes should be allowed to propagate to the private route table(s)"
  type = list(string)
  default = []
}

variable "public_propagating_vgws" {
  description = "A list of VGWs for which their routes should be allowed to propagate to the public route table(s)"
  type = list(string)
  default = []
}

variable "enable_dns_hostnames" {
  description = "Bool indicating whether you want to use DNS hostnames within the VPC"
  default     = false
}

variable "enable_dns_support" {
  description = "Bool indicating whether you want to use private DNS within the VPC"
  default     = true
}

variable "map_public_ip_on_launch" {
  description = "Bool indicating whether to auto-assign a public IP on launch"
  default     = true
}

variable "domain_name" {
  description = "Comma separated list of domain names to be used within the VPC"
  type = string
}

variable "domain_name_servers" {
  description = "List of domain name servers to be used within the VPC"
  type = list(string)
  default = ["127.0.0.1","AmazonProvidedDNS"]
}

variable "ntp_servers" {
  description = "List of NTP servers to be used inside the VPC (max 4)"
  type = list(string)
  default = []
}

variable "netbios_name_servers" {
  description = "List of NetBIOS servers to be used inside the VPC (max 4)"
  type = list(string)
  default = []
}

variable "netbios_node_type" {
  description = "NetBIOS node type. AWS recommend to specify 2 since broadcast and multicast are not supported"
  type = string
  default = 2
}
